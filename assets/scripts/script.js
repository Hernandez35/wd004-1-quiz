// console.log("hello");
// variables - data storage
// to decalare a variable, we'll use the keyword let or const!
// we will create the variable for the first time.
// let student = "Brandon";
// console.log(student);

const day = "Monday";
// console.log(day);

//const is short for constant
// we cannot reassign a value.

// day = "Tuesday";

student = "Brenda";
// console.log(student);

student = 3.1416;
// console.log(student);

// Data types 
//
// 1. String - denoted by quotation marks
//			- series of characters.
// 			Ex: "Brandon", "1234", "String" 

// 2. Number - can be used in mathematical operations.
// ex. 2, 3.15, 0.25, 450.


// 3. Boolean - true or false

console.log(typeof "Am I a string?"); //string

console.log(typeof "1234"); 
console.log(typeof 1234); //number
console.log(typeof "true"); //string
console.log(typeof false); //boolean
// console.log(typeof whatami?); undefined

//Operators 

// Arithmetic operators
// +, -, *, /

const num1 = 10;
const num2 = 2;
const num3 = 6;

console.log(num1 + num2); //12

//  % modulo --> remainder


// Assignment operator;
// = --> we are assigning a value to a variable.

// Assignment-Arithmetic
// +=, -=, *=, /=, %=

//
let output = 0;
const num4 = 5;
const num5 = 8;

output + output + num4; // 5 = 0 + 5
output += num4; // add num4 to the previous value of output.

// -= subtract num4 from the previous value

